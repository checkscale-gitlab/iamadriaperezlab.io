const getThemeIcon = () => {
    return document.querySelector('#theme i')
}

const getThemeSwitcher = () => {
    return document.querySelector('#theme')
}

const inDarkMode = () => {
    return document.documentElement.getAttribute('data-theme') == 'dark'
}

const switchTheme = (name) => {
    localStorage.setItem('theme', name)
    document.documentElement.setAttribute('data-theme', name);

    getThemeIcon().className = inDarkMode() ? 'far fa-lightbulb' : 'far fa-moon'
    getThemeSwitcher().title = inDarkMode() ? 'Light mode' : 'Dark mode'
}

getThemeSwitcher().addEventListener('click', function (e) {
    inDarkMode() ? switchTheme('light') : switchTheme('dark')
});

document.addEventListener('DOMContentLoaded', () => {
    const currentTheme = localStorage.getItem('theme')

    if (currentTheme) {
        switchTheme(currentTheme)
    }

    // Only enable transitions for the themes once the styles are loaded
    window.onload = () => document.body.classList.add('animated')
});
